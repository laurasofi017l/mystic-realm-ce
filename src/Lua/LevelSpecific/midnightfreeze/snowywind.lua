//can i? :pleading_face:
//~xelork

freeslot("MT_MFZWINDSPAWNER", "MT_MFZWINDSNOW")

freeslot("S_MFZWINDSNOW")

sfxinfo[sfx_s3kcel].flags = $|SF_NOMULTIPLESOUND

mobjinfo[MT_MFZWINDSPAWNER] = {
	--$Category Midnight Freeze
	--$Name Wind Particle Generator Start
	--$Sprite SNO1A0
	--$AngleText Angle/Tag
	doomednum = 36,
	spawnstate = S_INVISIBLE,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SCENERY,
}

mobjinfo[MT_MFZWINDSNOW] = {
	spawnstate = S_MFZWINDSNOW,
	flags = MF_NOCLIP|MF_NOTHINK,
}

states[S_MFZWINDSNOW] = {
	sprite = SPR_SNO1,
	frame = A,
	tics = -1,
	nextstate = S_MFZWINDSNOW,
}

/*
ok so this is how the data works:

object angle = object tag (every 360 degrees is a new tag) and particles direction
linedef tag must be the same as object tag for them to work together
and linedef action must be 6, to prevent conflict

control sector height = wind max distance in fracunits
linedef length = wind speed in fracunits
control sector special = wind spawn intermission in tics
control sector brightness = number of particles to spawn at once

linedef frontside x = second obj x in fracunits
linedef frontside y = second obj y in fracunits
control sector floor height = second obj z

the two objects will connect and create an "invisible linedef" that will generate the particles
the particles will fly, following the object angle, until they reach their max distance, specified by control sector height

YOU MUST CREATE AN INVISIBLE FOF AROUND THE SNOWING AREA WITH A LINEDEF EXECUTOR TO CALL THE LUA FUNCTION "SNOSFX" FOR THE SOUND!!!!!!!!!!!!!!!!!

thats it i guess
*/

addHook("LinedefExecute", function(l, mo)
	S_StartSound(mo.player.realmo, sfx_s3kcel, mo.player)
	mo.player.realmo.snowing = 17
end, "SNOSFX")

addHook("PlayerThink", function(p)
	if p.realmo.snowing == nil
		p.realmo.snowing = 0
	elseif (p.realmo.snowing)
		p.realmo.snowing = $ - 1
	else
		S_StopSoundByID(p.realmo, sfx_s3kcel)
	end
end)

addHook("MapThingSpawn", function(mo, mt)
	local trueangle = FixedAngle(mt.angle*FRACUNIT)
	mo.tag = mt.angle/360+1
	mo.particlelist = {}
	for ld in lines.iterate
		if ld.special == 6
		and ld.tag == mo.tag
			local ldd = R_PointToDist2(ld.v1.x, ld.v1.y, ld.v2.x, ld.v2.y)
			local ldsd = ld.frontside
			local ldst = ld.frontsector
			local target = P_SpawnMobj(ldsd.textureoffset, ldsd.rowoffset, ldst.floorheight, MT_MFZWINDSPAWNER)
			target.flags = MF_NOTHINK
			mo.target = target
			mo.windspeed = ldd/FRACUNIT
			mo.windmaxlen = abs(ldst.ceilingheight-ldst.floorheight)/FRACUNIT
			mo.timer = ldst.special
			mo.fuse = mo.timer
			mo.windamount = ldst.lightlevel
			mo.targetangle = R_PointToAngle2(max(mo.x, target.x), max(mo.y, target.y), min(mo.x, target.x), min(mo.y, target.y))
		end
	end
end, MT_MFZWINDSPAWNER)

addHook("MobjFuse", function(mo)
	for i = 1, mo.windamount
		local rx = P_RandomRange(mo.x/FRACUNIT, mo.target.x/FRACUNIT)*FRACUNIT
		local ry = P_RandomRange(mo.y/FRACUNIT, mo.target.y/FRACUNIT)*FRACUNIT
		local rz = P_RandomRange(mo.z/FRACUNIT, mo.target.z/FRACUNIT)*FRACUNIT
		if not (R_PointToAngle2(mo.x, mo.y, rx, ry) < mo.targetangle)
			local wind = P_SpawnMobj(rx, ry, rz, MT_MFZWINDSNOW)
			wind.windspeed = P_RandomRange(mo.windspeed-8, mo.windspeed)
			wind.frame = FF_FULLBRIGHT|P_RandomRange(0, 2)
			wind.timer = mo.windmaxlen/wind.windspeed
			table.insert(mo.particlelist, wind)
		end
	end
	mo.fuse = mo.timer
	return true
end, MT_MFZWINDSPAWNER)

addHook("MobjThinker", function(mo)
	for _,k in ipairs(mo.particlelist)
		if k.timer <= 0
			P_RemoveMobj(k)
			table.remove(mo.particlelist, _)
		else
			k.timer = $ - 1
			P_TryMove(k, k.x+FixedMul(cos(mo.angle),(k.windspeed*FRACUNIT)), k.y+FixedMul(sin(mo.angle),(k.windspeed*FRACUNIT)))
		end
	end
end, MT_MFZWINDSPAWNER)