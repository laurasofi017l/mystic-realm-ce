addHook("PlayerThink", function(p)
if not (p and p.mo and p.mo.valid and p.exiting and (gametyperules & GTR_FRIENDLY)) return end
	p.keepshield = p.powers[pw_shield]
end)

addHook("PlayerSpawn", function(p)
if p.keepshield == nil or not (gametyperules & GTR_FRIENDLY) return end
if p.mo.skin == "mario" or p.mo.skin == "skip" return end
if mapheaderinfo[gamemap].actnum == 1 return end
	p.powers[pw_shield] = p.keepshield
	P_SpawnShieldOrb(p)
	p.keepshield = nil
end)