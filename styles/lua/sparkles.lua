local function SafeFreeslot(...)
	for _, item in ipairs({...})
		if rawget(_G, item) == nil
			freeslot(item)
		end
	end
end

SafeFreeslot(
	"SPR_SUSK",
	"S_SUPERSPARK",
	"MT_SUPERSPARKLES"
)

states[S_SUPERSPARK] = {
       sprite = SPR_SUSK,
	   frame = FF_FULLBRIGHT|FF_ANIMATE|A,
	   var1 = 4,
	   var2 = 3
}
mobjinfo[MT_SUPERSPARKLES] = {
         spawnstate = S_SUPERSPARK,
         flags = MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT,
}

local colors = {SKINCOLOR_EMERALD, SKINCOLOR_PURPLE, SKINCOLOR_BLUE, SKINCOLOR_CYAN, SKINCOLOR_ORANGE, SKINCOLOR_RED, SKINCOLOR_GREY}

if MRCE_superSpark == nil then
	rawset(_G, "MRCE_superSpark", function(mo, amount, fuse, speed1, speed2, hyper, usecolor)
		local colors = {SKINCOLOR_EMERALD, SKINCOLOR_PURPLE, SKINCOLOR_BLUE, SKINCOLOR_CYAN, SKINCOLOR_ORANGE, SKINCOLOR_RED, SKINCOLOR_GREY}
		if not (mo and mo.valid and amount ~= nil and fuse ~= nil and speed1 ~= nil and speed2 ~= nil and hyper ~= nil) then
			return
		end

		for i = 0, amount, 1 do
			local ha = P_RandomRange(0, 360)*ANG1
			local va = P_RandomRange(0, 360)*ANG1
			local speed = P_RandomRange(speed1/FRACUNIT, speed2/FRACUNIT)*FRACUNIT

			local sprk = P_SpawnMobj(mo.x + FixedMul(FixedMul(mo.radius, FixedMul(cos(ha), cos(va))), mo.scale),
									 mo.y + FixedMul(FixedMul(mo.radius, FixedMul(sin(ha), cos(va))), mo.scale),
									 mo.z + FixedMul(FixedMul(mo.radius, sin(va)), mo.scale) + FixedMul(mo.scale, mo.height/2),
									 MT_SUPERSPARKLES)

			sprk.scale = mo.scale
			sprk.fuse = fuse
			if hyper == false then
				if usecolor == 0 then
					if mo.color then
						sprk.color = mo.color
					else
						sprk.color = 60
					end
				else
					sprk.color = usecolor
				end
			else
				sprk.color = colors[P_RandomRange(1, #colors)]
			end
			if mo.player and (mo.player == displayplayer) and not CV_FindVar("chasecam").value then
				sprk.flags2 = $|MF2_DONTDRAW
			end

			sprk.momx = FixedMul(FixedMul(speed, FixedMul(cos(ha), cos(va))), mo.scale)
			sprk.momy = FixedMul(FixedMul(speed, FixedMul(sin(ha), cos(va))), mo.scale)
			sprk.momz = FixedMul(FixedMul(speed, sin(va)), mo.scale)
		end
	end)
end

addHook("PlayerThink", function(p)
	if p and p.valid and p.mo and p.mo.valid
		and p.powers[pw_super]
		and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS5) 
		and not MRCE_isHyper(p) then
		MRCE_superSpark(p.mo, 1, 16, 1, 6*FRACUNIT, false, 0)
	end
end)